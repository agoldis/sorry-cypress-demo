const { defineConfig } = require("cypress");
const { currents } = require("cypress-cloud/plugin");

module.exports = defineConfig({
  e2e: {
    baseUrl: "https://en.wikipedia.org/",
    specPattern: "cypress/integration/*.spec.js",
    supportFile: "cypress/support/e2e.js",
    setupNodeEvents(on, config) {
      currents(on, config);
    },
  },
  projectId: "Ij0RfK",
  video: true,
  videoUploadOnPasses: false,
});
