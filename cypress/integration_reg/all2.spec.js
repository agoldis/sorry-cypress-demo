const retry =
  (fn, retriesCount = 5) =>
  async (...args) => {
    let i = retriesCount;
    let error;
    let result;
    while (i > 0) {
      try {
        error = null;
        result = await fn(...args);
        break;
      } catch (e) {
        error = e;
        i--;
      }
    }
    if (error) {
      throw error;
    }
    return result;
  };

let r = 0;

const beforeFn = () => {
  if (r < 3) {
    r++;
    console.log(r);
    throw new Error("error");
  }
  return "ok";
};

const runBefore = retry(beforeFn, 5);

const failOnTimeout = (timeout = 1000) => {
  let hasTimedOut = false;
  const _onRunnableRun = Cypress.runner.onRunnableRun;

  Cypress.runner.onRunnableRun = function (runnableRun, runnable, args) {
    // console.log({ runnableRun, runnable, args });
    const _args0 = args[0];

    args[0] = function () {
      console.log(runnable.type);
      if (runnable.type === "test" && hasTimedOut) {
        _args0(new Error("timeout"));
        return;
      }
      _args0();
    };
    _onRunnableRun.apply(this, [runnableRun, runnable, args]);
  };

  setTimeout(() => {
    // console.log(Cypress);
    // console.log(Cypress.spec);
    // console.log(Cypress.action);
    // console.log(Cypress.runner);
    // console.log(Cypress.currentTest);
    // Cypress.action("cypress:stop");
    // Cypress.runner.stop();
    // hasTimedOut = true;
  }, timeout);
};
describe("All tests", function () {

  let i = 0;
  it("Passed Test A", function () {
    cy.log(new Date().toLocaleString());
    cy.visit("/");
    cy.get("#simpleSearch").type("Africa");
    cy.get(".suggestions-result").first().click();
    cy.scrollTo(0, 1200);
    cy.contains("Africa");
  });
  it("Failed Test B", function () {
    cy.visit("/");
    cy.get("#simpleSearch").type("Africa");
    cy.get(".suggestions-result").first().click();
    cy.scrollTo(0, 1200);
    cy.contains("09999999933339");
  });
  xit("Pending Test B", function () {
    cy.visit("/");
    cy.get("#simpleSearch").type("Africa");
    cy.get(".suggestions-result").first().click();
    cy.scrollTo(0, 1200);
    cy.contains("i11111");
  });

  it(
    "Flaky test",
    {
      retries: 3,
    },
    function () {
      if (i > 1) {
        i--;
        throw new Error("oh!");
      }
      return;
    }
  );
});
