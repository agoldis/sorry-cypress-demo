describe("Skipped tests", function () {
  beforeEach(() => {
    throw new Error("Skipped tests");
  });
  it("Skipped Test A", function () {
    cy.log(new Date().toLocaleString());
    cy.visit("/");
    cy.get("#simpleSearch").type("Africa");
    cy.get(".suggestions-result").first().click();
    cy.scrollTo(0, 1200);
    cy.contains("Africa");
  });
  it("Skipped Test B", function () {
    cy.visit("/");
    cy.get("#simpleSearch").type("Africa");
    cy.get(".suggestions-result").first().click();
    cy.scrollTo(0, 1200);
    cy.contains("asdsadsadsada");
  });
});
