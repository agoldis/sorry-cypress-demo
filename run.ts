import { patch } from "cy2";
import cypress from "cypress";

process.env.CYPRESS_API_URL = "https://dashboard.service.url";

async function main() {
  // optional - provide cypress package main entry point location
  await patch(require.resolve("cypress"));

  cypress
    .run({
      // the path is relative to the current working directory
      spec: "./cypress/e2e/examples/actions.cy.js",
    })
    .then((results) => {
      console.log(results);
    })
    .catch((err) => {
      console.error(err);
    });
}
main().catch((error) => {
  console.error(error);
  process.exit(1);
});
